    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!--<a class="navbar-brand" href="#">Biblioteka</a>-->
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
              
              <%

                                String str = request.getParameter("nav");

                                if(str != null && str.equals("user"))
                                {
                                    out.println(" class='active'");
                                }  
                }
                %>

            <!--   <li <% String str = request.getParameter("nav");  (str!= null && str.equals("user")){(" class='active'");}: %><a href="/JSP/index.do?nav=user">User</a></li> -->
            <li><a href="/JSP/index.do?nav=books">Books</a></li>
            <li><a href="/JSP/index.do?nav=borrowed">Borrowed</a></li>
            <li class='active'><a href="/JSP/index.do?nav=shopping">Shopping cart</a></li>
        
        </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>