<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
    

    <c:if test="${param.navigation eq null}">  
        <c:set var="currentPage" scope="session" value="${Integer(1)}"/>
        <% request.getRequestDispatcher("showPost.do?navigation=post").forward(request, response); %>
    </c:if>    
          
<%@include file='web_parts/headerHTML.jsp'%>

  <body>
      
    <%@include file='web_parts/navigation.jsp'%>    
    

    <div class="container">

        <%@include file='web_parts/header.jsp'%>
        
      <div class="row">

        <%@ include file='web_parts/main.jsp'%>

        <%@include file='web_parts/sidebar.jsp'%>

      </div><!-- /.row -->

    </div><!-- /.container -->

    <%@include file='web_parts/footer.jsp'%>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="${pageContext.request.contextPath}/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="${pageContext.request.contextPath}/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="${pageContext.request.contextPath}/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
