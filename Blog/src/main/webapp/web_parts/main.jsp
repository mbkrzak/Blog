<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="col-sm-8 blog-main">

    <h1> Aktualna strona: <c:out value="${currentPage}" /> </h1>

    
    
    <c:if test="${param.navigation eq 'post'}">  
       <c:import url='web_parts/main_parts/showPosts.jsp'/> 
    </c:if>  
    
    <c:if test="${param.navigation eq 'add'}">  
       <c:import url='web_parts/main_parts/addPost.jsp'/>
    </c:if>  
    
    <c:if test="${param.navigation eq 'author'}">  
       <c:import url='web_parts/main_parts/addUser.jsp'/> 
    </c:if> 
  
    <nav>                        
        <ul class="pager">                                 
            <c:if test="${param.navigation eq 'post'}">  
                
                <c:if test="${currentPage > 1}">                        
                    <c:if test="${param.year ne null && param.year ne null}">                                            
                        <li><a href="<c:out value="showPost.do?changePage=previous&year=${param.year}&month=${param.month}"/>">Next</a></li>                     
                    </c:if>
                    <c:if test="${param.year eq null ||param.year eq null}">                                            
                        <li><a href="showPost.do?changePage=previous">">Next</a></li>                     
                    </c:if>                  
                </c:if>  
                           
                <c:if test="${currentPage < nrPages}">                     
                    <c:if test="${param.year ne null && param.year ne null}">                                            
                        <li><a href="<c:out value="showPost.do?changePage=next&year=${param.year}&month=${param.month}"/>">Next</a></li>                     
                    </c:if>
                    <c:if test="${param.year eq null ||param.year eq null}">                                            
                        <li><a href="showPost.do?changePage=next">Next</a></li>                     
                    </c:if>                        
                </c:if>    
                    
            </c:if>              
        </ul>                
    </nav>

    

</div><!-- /.blog-main -->