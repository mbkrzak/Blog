<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<div class="blog-masthead">      
    <div class="container">
        
        <nav class="blog-nav">
                
            <a class="blog-nav-item <%= (request.getParameter("navigation").equals("post"))? "active": ""%>" href="showPost.do?navigation=post">Posty</a>                
            <a class="blog-nav-item <%= (request.getParameter("navigation").equals("add"))? "active": ""%>" href="index.jsp?navigation=add">Dodaj post</a>
            <a class="blog-nav-item <%= (request.getParameter("navigation").equals("author"))? "active": ""%>" href="index.jsp?navigation=author">Dodaj autora</a>       

        </nav>      
    </div>    
</div>
