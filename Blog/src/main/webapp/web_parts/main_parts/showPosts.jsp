<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>          
<div class="blog-post">
    
    <c:forEach items="${posts}" var="post">
    
        <h2 class="blog-post-title">${post.getTitle()}</h2>             
        <p class="blog-post-meta"> ${post.getCreateDate()} <a href="#">${post.getUser().getUsername()}</a></p>    
        <p>${post.getText()}</p>
        
    </c:forEach>  
          
</div><!-- /.blog-post -->