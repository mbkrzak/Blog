<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<c:if test="${param.isUser eq 'false'}">         
    <%= "<h4> <font color='red'>Nie mo?na doda? postu - nie znaleziono u?ytkownika! </font></h4>" %>
</c:if>  
<c:if test="${param.error eq 'false'}">         
    <%= "<h4> <font color='green'>Dodanie u?ytkownika powiod?o si?!</font></h4>" %>
</c:if> 
<c:if test="${param.error eq 'true'}">         
    <%= "<h4> <font color='red'>"+ session.getAttribute("errorMessage")+"</font></h4>" %>
</c:if> 

<div style="width:200px;">
    <form name="form" action="/Blog/addUser.do" method="POST">
        <div class="row">
            <input class="form-control"  placeholder="Imi?" type="text" name="firstname" value="" />
        </div>
        <br/>
        <div class="row">
            <input class="form-control" placeholder="Nazwisko" type="text" name="lastname" value="" />
        </div>
        <br/>
        <div class="row">
            <input class="form-control" placeholder="Nazwa u?ytkownika" type="text" name="username" value="" />
        </div>
        <br/>
        <div class="row">
        <input class="btn btn-default" type="submit" value="Dodaj" />
        </div>
</div><!-- /.blog-post -->