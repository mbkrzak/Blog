/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.web.model.Post;
import pl.sda.web.model.User;

/**
 *
 * @author RWSwiss
 */
public class AddUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try
        {
            addUser(request, response);
            request.getRequestDispatcher("/index.jsp?navigation=author&error=false").forward(request, response);

        }
        catch(IllegalStateException e)
        {
            HttpSession session = request.getSession();    
            session.setAttribute("errorMessage", e.getMessage());
            request.getRequestDispatcher("/index.jsp?navigation=author&error=true").forward(request, response);
        }

        
    }

        private void addUser(HttpServletRequest request, HttpServletResponse response)
    {
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();     
         
        String firstName = request.getParameter("firstname");
        String lastName = request.getParameter("lastname");    
        String login = request.getParameter("username");        

        Query query = session.createQuery("From User where username= :username");
        query.setParameter("username", login);
        List<User> list = query.list();
        Optional<User> optionalUser = list.stream().findFirst();

        
        if(!optionalUser.isPresent())
        {
            //User setting
            User user = new User();
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setUsername(login);

            session.save(user);
            beginTransaction.commit();
        }
        else
        {
            throw new IllegalStateException("User " + login + " already exist!");
        }       
        
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
