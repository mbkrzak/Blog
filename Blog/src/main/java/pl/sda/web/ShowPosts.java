/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.web.model.Post;
import pl.sda.web.model.User;

/**
 *
 * @author RWSwiss
 */
public class ShowPosts extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        List<Post> posts = allPosts(request, response);  
        List<SideBarDate> dates = monthlyPosts(posts);             
    
        String year =request.getParameter("year");
        String month = request.getParameter("month");
        
        if( year != null &&  month != null )
        {
            posts = postsByMonth(Integer.parseInt(year), Integer.parseInt(month), posts);
        }
        
        HttpSession session = request.getSession();

        Integer currentPage = (Integer)session.getAttribute("currentPage");
        int nrPages = (int) Math.ceil(posts.size()/3.0);
        
        String changePage = request.getParameter("changePage");
        
        if(changePage != null){
            if( changePage.equals("previous") && currentPage>1){
                currentPage--;
            }
            else if(changePage.equals("next")&& currentPage<nrPages){
                currentPage++;
            }
        }
        else
        {
            currentPage= 1;
        }
        List<Post> tempPostList = posts.stream().skip((currentPage-1)*3).collect(Collectors.toList());
        if (tempPostList.size()>=3)
        {
            posts = tempPostList.subList(0, 3);
        }
        else
        {
            posts = tempPostList;
        }
  
        session.setAttribute("nrPages", new Integer(nrPages));        
        session.setAttribute("currentPage", new Integer(currentPage));
        session.setAttribute("posts", posts);
        session.setAttribute("sideBarDate", dates);
        

       


        request.getRequestDispatcher("/index.jsp?navigation=post").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private List<Post> allPosts(HttpServletRequest request, HttpServletResponse response) {
                
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();
        
        Query query = session.createQuery("From Post order by timestamp(createDate) desc");
        List<Post> posts = query.list();
        
        return posts;
     
    }
    
    private List<Post> postsByMonth(int year, int month, List<Post> posts)
    {
        List<Post> reducedPosts = new ArrayList<>();
        
        for (Post post : posts)
        {
            Calendar cal = Calendar.getInstance();
            cal.setTime(post.getCreateDate());
            int yearPost = cal.get(Calendar.YEAR);
            int monthPost = cal.get(Calendar.MONTH)+1;
            
            if(monthPost == month && yearPost == year)
            {
                reducedPosts.add(post);
            }
        }
        
        return reducedPosts;
    }
    
    private List<SideBarDate> monthlyPosts(List<Post> posts)
    {
        List<SideBarDate> sbPosts = new ArrayList<>();
        List<Date> dates = posts.stream().map(t -> t.getCreateDate()).collect(Collectors.toList());
        
        for (Date date : dates) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH)+1;
            
            SideBarDate sideBarDate = new SideBarDate(year, month);
            
            Optional<SideBarDate> duplicatedDate = sbPosts.stream().
                    filter(t ->(t.getIntMonth() == sideBarDate.getIntMonth())&&                        
                    (t.getYear()== sideBarDate.getYear())).findFirst();
            
            if(!duplicatedDate.isPresent())
            {
                sbPosts.add(sideBarDate);
            }
        }               
        
        return sbPosts;
    }

}
