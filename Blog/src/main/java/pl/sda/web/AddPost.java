/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web;
import pl.sda.web.model.User;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import pl.sda.web.model.Post;

/**
 *
 * @author Maciek
 */
public class AddPost extends HttpServlet {
    
    private SessionFactory sessionFactory;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        Optional<User> user = checkUserExist(request.getParameter("username"));
        
        if(user.isPresent())
        {
            addPost(request, response, user.get());
            request.getRequestDispatcher("/index.jsp?navigation=post").forward(request, response);
        }
        else
        {
            request.getRequestDispatcher("/index.jsp?navigation=author&isUser=false").forward(request, response);
        }
        
    }
    
    private Optional<User> checkUserExist(String username)
    {
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();
        
        Query query = session.createQuery("From User where username= :username");
        query.setParameter("username", username);
        List<User> list = query.list();
        
        //Optional user = list.get(0);
        Optional<User> user = list.stream().findFirst();
        
        
        return user;
       
    }
    
    private void addPost(HttpServletRequest request, HttpServletResponse response, User user)
    {
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction beginTransaction = session.beginTransaction();     
         
        String title = request.getParameter("title");
        String text = request.getParameter("text");        
        Date date = new Date(new java.util.Date().getTime());       
        
        //Post setting
        Post post = new Post();
        post.setTitle(title);
        post.setText(text);
        post.setCreateDate(date);
        post.setUser(user);
        
        session.save(post);
        beginTransaction.commit();
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
