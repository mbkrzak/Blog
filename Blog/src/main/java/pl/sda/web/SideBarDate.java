/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.web;

/**
 *
 * @author RWSwiss
 */
public class SideBarDate {
    private final int year;
    private final int month;
    
    public SideBarDate(int year, int month)
    {
        this.year = year;
        this.month = month;        
    }

    public int getYear() {
        return year;
    }
    
    public int getIntMonth()
    {
        return month;
    }

    public String getStringMonth() {
        
        String monthString;
        
        switch (month) {
            case 1:  monthString = "Styczeń";
                     break;
            case 2:  monthString = "Luty";
                     break;
            case 3:  monthString = "Marzec";
                     break;
            case 4:  monthString = "Kwiecień";
                     break;
            case 5:  monthString = "Maj";
                     break;
            case 6:  monthString = "Czerwiec";
                     break;
            case 7:  monthString = "Lipiec";
                     break;
            case 8:  monthString = "Sierpień";
                     break;
            case 9:  monthString = "Wrzesień";
                     break;
            case 10: monthString = "Październik";
                     break;
            case 11: monthString = "Listopad";
                     break;
            case 12: monthString = "Grudzień";
                     break;
            default: monthString = "Invalid month";
                     break;                     
        }
        
        
        return monthString;
    }    
    
}
