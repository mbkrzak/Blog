/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda;
import pl.sda.model.Book;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;



/**
 *
 * @author RWSwiss
 */
@ManagedBean(name = "bookManagedBean")
@RequestScoped
public class BookManagedBean {
    
    @EJB
    BookSessionBeanLocal bookSessionBeanLocal;

    BookDto bookDto = new BookDto();

    public BookDto getBookDto() {
        return bookDto;
    }

    public void setBookDto(BookDto bookDto) {
        this.bookDto = bookDto;
    }
    
    public void save()
    {
        System.out.println("Saved book: title '" + bookDto.getTitle() 
                + "' ISBN: " + bookDto.getIsbn() + " release date: " + bookDto.getReleaseDate());
        Book book = new Book();
        book.setIsbn(bookDto.getIsbn());
        book.setReleaseDate(bookDto.getReleaseDate());
        book.setTitle(bookDto.getTitle());
        
        bookSessionBeanLocal.addBook(book);
        
    }
}
