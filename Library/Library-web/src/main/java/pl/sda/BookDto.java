/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda;

/**
 *
 * @author RWSwiss
 */
public class BookDto {
    private String title;
    private String isbn;
    private int releaseDate;

    public String getTitle() {
        return title;
    }

    public String getIsbn() {
        return isbn;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setReleaseDate(int releaseDate) {
        this.releaseDate = releaseDate;
    }


}
