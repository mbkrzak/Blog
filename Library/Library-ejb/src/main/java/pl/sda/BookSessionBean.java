/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.*;
import pl.sda.model.Book;

/**
 *
 * @author RWSwiss
 */
@Stateless
public class BookSessionBean implements BookSessionBeanLocal {

    @PersistenceContext(unitName = "pu")
    private EntityManager em;

    
    @Override
    public void addBook(Book book) {
        
        EntityTransaction et = em.getTransaction();
        et.begin();
        em.persist(book);
        et.commit();
        
    }

    @Override
    public List<Book> getBooks() {
        /*
        EntityTransaction et = em.getTransaction();
        et.begin();
        Query query = em.createQuery("From Book");        
        et.commit();
        return query.getResultList();
        */
        List<Book> books = new ArrayList<>();
        return books;
        
    }

}
