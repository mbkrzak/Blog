/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda;

import java.util.List;
import javax.ejb.*;
import pl.sda.model.Book;

/**
 *
 * @author RWSwiss
 */
@Local
@Remote
public interface BookSessionBeanLocal {
    public void addBook(Book book);
    public List<Book> getBooks();
}
